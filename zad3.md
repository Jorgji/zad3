---
author: Enea Jorgji
title: Albania
subtitle: Beamer
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Spis treści
1. Wprowadzenie
2. Historia
    - Od starożytności do I wojny światowej
    - Okres wojen światowych
    - Okres powojenny
3. Turystyka
4. Kuchnia

## 1. Wprowadzenie
**Albania** Republika Albanii *(alb. Shqipëria, Republika e shqipërise)* - państwo w południowo-wschodniej Europie na Bałkanach. Albania na zachodzie ma dostęp do Morza Adriatyckiego, a na południowym zachodzie do Morza Jońskiego.

## 2. Historia - Od starożytności do I wojny światowej
W starożytności Albania wchodziła w skład Ilirii zamieszkiwanej przez lud indoeuropejski Ilirów, którzy wymieszali się później z Trakami i Słowianami. Około 1435 Turcy zajęli Albanię. Wyzwoliła się ona czasowo po powstaniu zainicjowanym przez Skanderbega. Po uzyskaniu niepodległości przez Albanię, przy aprobacie państw Europy Zachodniej, rozpoczęto poszukiwania arystokraty zdolnego objąć albański tron.

## 2. Historia - Okres wojen światowych 
Albania była okupowana przez Serbów, a potem podążających za nimi Austriaków.

Pod koniec wojny wkroczyli Włosi, chcący włączyć kraj w swoją strefę wpływów, ale ponieśli porażkę w walkach z albańskimi powstańcami.

Konferencja z 1920 zatwierdziła niepodległość Albanii (ujął się za nią Woodrow Wilson). Zdetronizowano Wilhelma zu Wieda (nie ogłosił abdykacji) i proklamowano republikę. W 1928 konserwatywny premier Ahmed Zogu koronował się na króla, wiążąc kraj politycznie i militarnie, a nade wszystko gospodarczo z Włochami Benita Mussoliniego. Włochy zajęły Albanię w kwietniu 1939 roku, łamiąc swe zobowiązania międzynarodowe i obalając króla. 16 kwietnia 1939 roku król Włoch, Wiktor Emanuel III, został ogłoszony królem Albanii.

## 2. Historia - Okres powojenny
W 1946 roku powstała Ludowa Republika Albanii. W tym samym roku doszło do incydentu w cieśninie Korfu wywołanego uszkodzeniem brytyjskich okrętów na postawionych przez Albanię minach. Gdy w 1948 doszło do rozłamu w bloku wschodnim, opowiedział się za Związkiem Radzieckim. W 1985 roku zmarł Enver Hoxha, a władzę w Albanii przejął Ramiz Alia, uznawany za członka liberalnej frakcji wewnątrz Partii Pracy. Dwa lata później w kraju rozpoczął się kryzys gospodarczy, pogarszał się albański bilans handlowy, pojawiły się trudności z zaopatrzeniem w podstawowe produkty.

## 3. Turystyka
Po długotrwałej izolacji Albania koncentruje się na rozwoju turystyki międzynarodowej, głównie na wybrzeżu Riwiery Albańskiej. W 2016 roku kraj ten odwiedziło 4,070 mln turystów (7,5% więcej niż w roku poprzednim), generując dla niego przychody na poziomie 1,691 mld dolarów.

## 3.1. Turystyka - Berat
![Berat](image/Berat.jpg "title-1") 

## 3.2. Turystyka - Gjirokastra
![Gjirokastra](image/gjirokastra.jpg "title-2")

## 3.3. Turystyka - Saranda
![Saranda](image/plaza.jpeg)

## 3.4. Turystyka - Banjat e Benjes - gorące żródła
![Banjat e Benjes - gorące żródła](image/banjat_e_benjes.jpg)

## 4. Kuchnia
Kuchnia Albanii swymi walorami przypomina kuchnie innych krajów bałkańskich, wzbogacone przez wpływy tureckie. Jedną z najpopularniejszych potraw jest qofte, przyrządzane z mięsa siekanego lub mielonego. Innymi potrawami chętnie jadanymi przez Albańczyków są:
- mięso pieczone (mish i përzier),
- ziemniaki (patate) z jarzynami,
- różne gatunki sera (djathë),
- oriz më tamel, czyli pudding ryżowy na owczym mleku.

## 4. Kuchnia - Zdjęcia
![zapiekanka](image/potrawy.JPG) 
![zapiekanka](image/potrawy_1.JPG)